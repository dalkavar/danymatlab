%% GUIDE initialization code
function varargout = WallExtraction(varargin)
	addpath('imgzoompan');
	gui_Singleton = 1;
	gui_State = struct('gui_Name',       mfilename, ...
					   'gui_Singleton',  gui_Singleton, ...
					   'gui_OpeningFcn', @WallExtraction_OpeningFcn, ...
					   'gui_OutputFcn',  @WallExtraction_OutputFcn, ...
					   'gui_LayoutFcn',  [] , ...
					   'gui_Callback',   []);
	if nargin && ischar(varargin{1})
		gui_State.gui_Callback = str2func(varargin{1});
	end

	if nargout
		[varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
	else
		gui_mainfcn(gui_State, varargin{:});
	end
% End initialization code - DO NOT EDIT
end

% --- Executes just before WallExtraction is made visible.
function WallExtraction_OpeningFcn(hObject, eventdata, handles, varargin)
	clc;
	handles.output = hObject;
	
	% store config variables in frmMain UserData (Class type Config)
	handles.frmMain.UserData.config = Config;
	cfg = handles.frmMain.UserData.config;
	
	guidata(hObject, handles);
	
	% if we got a path as argument, immediately run full method on input image
	if (numel(varargin) > 0)
		cfg.imgFromArgument = true;
		cfg.imgPath = varargin{1};
		runFullMethod(cfg, handles);
	else
		% no arguments pre-loads last data used
		cfg.imgFromArgument = true;
		loadImage('walls/assets/wall-input.png', handles);
		segmentData = loadSegments(handles);
		drawSegments(segmentData, cfg, handles);
		refreshRooms(cfg, handles);
	end
	
	uiwait(handles.frmMain);
end

% --- Outputs from this function are returned to the command line.
function varargout = WallExtraction_OutputFcn(hObject, eventdata, handles)
	%varargout{1} = handles.output; % Get default command line output from handles structure
end



%% ToolButton Callbacks

% ToolButton: Open image file
function btOpenImage_ClickedCallback(hObject, eventdata, handles)
	cfg = handles.frmMain.UserData.config;
	hForm = gcf;
	
	[name, path] = uigetfile({'*.png;*.tif;*.bmp', 'Image file (*.png, *.tif, *.bmp)'}, 'Select the floor plan image');
	if name == 0
		return
	end
	fullPath = fullfile(path, name);
	cfg.imgPath = fullPath;
	runFullMethod(cfg, handles);
end

% ToolButton: Open segments file
function btOpenSegments_ClickedCallback(hObject, eventdata, handles)
	[name, path] = uigetfile({'*.txt', 'Text file (*.txt)'}, 'Select the segments file');
	if name == 0
		return
	end
	
	loadSegments(fullfile(path, name), handles);
end

% ToolButton: Save current segments to a text file and update rooms
function btSaveSegments_ClickedCallback(hObject, eventdata, handles)
	cfg = handles.frmMain.UserData.config;
	saveSegments(handles);
	refreshRooms(cfg, handles);
end

function btWallExtraction_ClickedCallback(hObject, eventdata, handles)
	cfg = handles.frmMain.UserData.config;
	runWallExtraction(cfg, handles);
end

%% Drawing area graphic object callbacks ======

% axes image mouse callback
function axImage_ButtonDownFcn(hObject, event, handles)
	% On left click, de-select any selected line
	if (event.Button == 1)
		cfg = hObject.Parent.Parent.UserData.config;
		if ~isempty(cfg.hSelectedPoly)
			cfg.hSelectedPoly.deselect();
		end
		
		% update rooms from scratch!
		%refreshRooms(cfg, handles);
	end
end


% --- Executes on key press with focus on frmMain and none of its controls.
function frmMain_KeyPressFcn(hObject, event, handles)
	cfg = handles.frmMain.UserData.config;
	if strcmp(event.Key, 'delete')
		if ~isempty(cfg.hSelectedPoly)
			delete(cfg.hSelectedPoly);
			cfg.hSelectedPoly = gobjects(0);
		end
	end
end


%% General Functionality ===================================================================================================

% opens image, shows it, shows waitBar, runs wallExtraction and roomSegmentation, display everything
function runFullMethod(cfg, handles)
	%wb = waitbar(0.0, 'Loading image...', 'WindowStyle', 'modal');
	loadImage(cfg.imgPath, handles);
	
	%waitbar(0.3, wb, 'Performing wall extraction...');
	status = runWallExtraction(cfg);
	if (status == 0)
		segmentData = loadSegments(handles);
		drawSegments(segmentData, cfg, handles);
		
		%waitbar(0.7, wb, 'Performing room segmentation...');
		refreshRooms(cfg, handles);
		%close(wb);
	else
		uiwait(msgbox('An error ocurred while attempting to extract walls. Please try a different image selection.', 'Error', 'error'));
	end
	
	
end

% opens and displays the image at path, prepares UI to display image with zoom
function loadImage(path, handles)
	img = imread(path);
	[h, w, ~] = size(img);
	
	% Update configuration
	cfg = handles.frmMain.UserData.config;
	cfg.loaded = 1;
	cfg.imgPath = path;
	cfg.imgWidth = w;
	cfg.imgHeight = h;
	
	% Display with zoom & pan
	if (cfg.imgFromArgument)
		axesChildHandle = imshow((1-img)*255);
	else
		axesChildHandle = imshow(img);
	end
	imgzoompan(handles.axImage, 'ImgWidth', w, 'ImgHeight', h, 'Magnify', 2, 'MaxZoomScrollCount', 6);
	axesChildHandle.ButtonDownFcn = {@axImage_ButtonDownFcn, handles};
end

% opens segments from a file
function segmentData = loadSegments(handles)
	fprintf('Loading wall line segments... ');	
	segmentData = csvread('walls/assets/segments.txt');
	fprintf('Done.\n');	
end

% saves current segments into segments.txt file
function saveSegments(handles)
	f = fopen('walls/assets/segments.txt', 'w');
	patches = findobj(handles.axImage, 'Tag', 'line');
	[n, ~] = size(patches);
	for i = 1 : n
		hLinePatch = patches(i).UserData.Creator;
		line = hLinePatch.serialize();
		fprintf(f, '%s\n', line);
	end
	fclose(f);	
end

% clear previous lines and re-draw line segments
function drawSegments(segmentData, cfg, handles)
	% remove any existing line segment
	patches = findobj(handles.axImage, 'Tag', 'line');
	[n, ~] = size(patches);
	for i = 1 : n
		hLinePatch = patches(i).UserData.Creator;
		delete(hLinePatch);
	end
	
	% Draw loaded segments using LinePatch
	[rows, ~] = size(segmentData);
	of = 1.0; % offset for coordinates
	for i = 1 : rows
		row = segmentData(i, :);
		normal = [row(7), row(8)];
		LinePatch(handles.axImage, row(1) + of, row(2) + of, row(3) + of, row(4) + of, row(5), row(6), normal, row(9), cfg);
	end
end

% loads room walls and room wall sets from default file paths
% x0 and y0 are offsets applied to all coordinates
% reuse at prototype!
function [wallData, roomData] = loadRooms(x0, y0)
	fprintf('Loading detected rooms... ');
	of = 1.0; % offset for point coordinates
	
	% load walls
	data = csvread('walls/assets/room-walls.txt');
	[rows, ~] = size(data);
	for i = 1 : rows
		row = data(i, :);
		endpoints = [row(2) + x0, row(3) + y0 ; row(4) + x0, row(5) + y0] + of;
		wallData(i) = struct('number', row(1), 'type', 'Wall', 'endpoints', endpoints);
	end
	
	% load rooms
	fid1 = fopen('walls/assets/rooms.txt','r');
	i = 1;
	while ~feof(fid1)
		line = fgets(fid1);
		tokens = str2num(line);
		wallList = tokens(2:end);
		roomData(i) = struct('number', tokens(1), 'walllist', wallList, 'polyregion', []);
		
		% fill polyregion array with region points
		points = [wallData(wallList).endpoints];
		X = points(:, 1:2:end);
		Y = points(:, 2:2:end);
		X = reshape(X, [], 1);
		Y = reshape(Y, [], 1);
		roomData(i).polyregion = [X, Y];
		i = i + 1;
	end
	fclose(fid1);
	fprintf('Done.\n');	
end

% Reload rooms from scratch and show (redraw) them on the window
function refreshRooms(cfg, handles)
	deleteRoomPatches(handles);
	
	status = runRoomSegmentation(cfg);
	if (status == 0)
		[wallData, roomData] = loadRooms(0, 0);
		drawRooms(handles, roomData);
	end
end

% Draws the room areas using the wallData and roomData obtained previously
function drawRooms(handles, roomData)
	colors = [ 230, 25, 75; 60, 180, 75; 255, 225, 25; 0, 130, 200; 245, 130, 48; 145, 30, 180; 240, 50, 230; 136, 86, 167; 0, 128, 128; 170, 110, 40; 128, 0, 0; 128, 128, 0; 0, 0, 128; 100,50,0; 140,94,0; 76,65,19 ];
	colors = colors ./ 255;
	[~, cols] = size(roomData);
	
	numColor = 1;
	for i = 1 : cols
		X = roomData(i).polyregion(:, 1);
		Y = roomData(i).polyregion(:, 2);
		patch(X, Y, colors(numColor, :), 'Parent', handles.axImage, 'Tag', 'room', ...
			'EdgeColor', 'none', 'FaceAlpha', 0.45, 'PickableParts', 'none', 'HitTest', 'off');
		
		numColor = numColor + 1;
		if (numColor > 16), numColor = 1; end
	end
end

% Deletes all room patches
function deleteRoomPatches(handles)
	patches = findobj(handles.axImage, 'Tag', 'room');
	delete(patches);
end

% Runs the WallExtraction program in the file at imgPath
% Generates line segments at segments.txt
function status = runWallExtraction(cfg)
	currentPath = pwd;
	exePath = [currentPath, '/walls/bin/walls'];
	command = ['"', exePath, '" --img ', cfg.imgPath, ' --out ', 'walls/assets/'];
    
	fprintf('Executing: %s\n', command);
	[status, cmdout] = dos(command);
	fprintf('WallExtraction finished with status code %d\n', status);
	if (status ~= 0)
		fprintf('Output: %s\n', cmdout);
	end
end

% Runs the room segmentation program
function status = runRoomSegmentation(cfg)
	currentPath = pwd;
	exePath = [currentPath, '/walls/bin/rooms'];
	command = ['"', exePath, '" --out ', 'walls/assets/'];
    
	fprintf('Executing: %s\n', command);
	status = unix(command);
	fprintf('RoomSegmentation finished with status code %d\n', status);
end
